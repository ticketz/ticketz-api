#!/usr/bin/env bash

sudo mkdir -p /var/lib/postgresql/ticketz-data; sudo chown $(whoami) /var/lib/postgresql/ticketz-data
# you may need to mount `/var/lib/postgresql/ticketz-data` in docker

docker-compose -f etc/docker/supporting-services.yml -p ticketz-supporting-services up -d
