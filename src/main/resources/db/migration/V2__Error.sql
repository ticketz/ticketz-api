create table if not exists error
(
    id            int8 not null,
    created_by    int8,
    created_date  timestamp,
    deleted       boolean default false,
    modified_by   int8,
    modified_date timestamp,
    key           text,
    message_ar    text,
    primary key (id)
);
