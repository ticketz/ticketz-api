create table if not exists lk_city
(
    id            int8 not null,
    created_by    int8,
    created_date  timestamp,
    modified_by   int8,
    modified_date timestamp,
    display_order int4,
    name_ar       text,
    primary key (id)
);

insert into lk_city (id, name_ar)
values (1, 'الرياض'),
       (2, 'جدة'),
       (3, 'الدمام');
----
create sequence if not exists seq_location start 1 increment 1;
create table if not exists location
(
    id             int8 not null default nextval('seq_location'),
    created_by     int8,
    created_date   timestamp,
    modified_by    int8,
    modified_date  timestamp,
    description_ar text,
    latitude       float8,
    longitude      float8,
    city_id        int8,
    primary key (id),
    foreign key (city_id) references lk_city (id)
);

----
create sequence if not exists seq_event start 1 increment 1;
create table if not exists event
(
    id             int8      not null default nextval('seq_event'),
    created_by     int8,
    created_date   timestamp,
    modified_by    int8,
    modified_date  timestamp,
    title_ar       text      not null,
    description_ar text,
    datetime       timestamp not null,
    ticket_count   int4      not null,
    price          float8    not null,
    location_id    int8,
    primary key (id),
    foreign key (location_id) references location (id)
);

create table event_actors
(
    event_id      int8 not null,
    actor_name_ar text,
    foreign key (event_id) references event (id)
);

create table event_images
(
    event_id      int8 not null,
    display_order int4,
    url           text,
    foreign key (event_id) references event (id)
);
---
create sequence if not exists seq_reservation start 1 increment 1;
create table reservation
(
    id            int8 not null default nextval('seq_reservation'),
    created_by    int8,
    created_date  timestamp,
    modified_by   int8,
    modified_date timestamp,
    email         text,
    mobile_number text,
    paid          boolean       default false,
    ticket_count  int4 not null,
    event_id      int8,
    primary key (id),
    foreign key (event_id) references event (id)
);
--
create sequence if not exists seq_payment start 1 increment 1;
create table payment
(
    id             int8 not null default nextval('seq_payment'),
    created_by     int8,
    created_date   timestamp,
    modified_by    int8,
    modified_date  timestamp,
    datetime       timestamp,
    gateway_name   text,
    reservation_id int8,
    primary key (id),
    foreign key (reservation_id) references reservation (id)
);
---
