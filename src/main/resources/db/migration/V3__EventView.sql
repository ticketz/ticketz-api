create or replace view vw_event as
select e.id,
       e.title_ar,
       e.datetime,
       (select string_agg(actor_name_ar, '، ') from event_actors where event_id = e.id) actor_names_ar,
       (select url
        from event_images
        where event_id = e.id
          and display_order =
              (select min(display_order) from event_images where event_id = e.id))      image,
       l.city_id,
       lc.name_ar                                                                       city_name_ar
from event e
         left join location l on e.location_id = l.id
         left join lk_city lc on l.city_id = lc.id;
