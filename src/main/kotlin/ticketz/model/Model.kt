package ticketz.model

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.NoRepositoryBean
import java.time.Instant
import javax.persistence.*
import javax.persistence.AttributeOverride as Attr
import javax.persistence.AttributeOverrides as Attrs

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractModel(
        @CreatedBy var createdBy: Long? = null,
        @CreatedDate var createdDate: Instant? = Instant.now(),
        @LastModifiedBy var modifiedBy: Long? = null,
        @LastModifiedDate var modifiedDate: Instant? = null,
        @Id var id: Long? = null
)

@Embeddable
data class Name(var ar: String)

@MappedSuperclass
abstract class AbstractLookup(
        @Embedded
        @Attrs(Attr(name = "ar", column = Column(name = "nameAr")))
        var name: Name? = null,
        var displayOrder: Int? = null
) : AbstractModel()

@NoRepositoryBean
interface LookupRepository<T : AbstractLookup> : JpaRepository<T, Long> {

    @Query("select o from #{#entityName} o order by o.displayOrder, o.id")
    override fun findAll(): MutableList<T>
}
