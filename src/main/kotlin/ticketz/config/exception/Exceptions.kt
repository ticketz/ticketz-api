package ticketz.config.exception

object NotFoundException : Exception("resource.not.found")

class ClientException(key: String) : Exception(key)

class ServerException(key: String = "internal.server.error") : Exception(key)

