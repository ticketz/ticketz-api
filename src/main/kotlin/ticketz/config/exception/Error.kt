package ticketz.config.exception

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.http.HttpStatus
import ticketz.model.AbstractModel
import ticketz.model.Name
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.AttributeOverride as Attr
import javax.persistence.AttributeOverrides as Attrs

@Entity
data class Error(
        var key: String,
        @Attrs(Attr(name = "ar", column = Column(name = "messageAr")))
        var message: Name
) : AbstractModel() {
    fun toDto(status: Int) = ErrorDto(key, message, status)
    fun toDto(status: HttpStatus) = toDto(status.value())
}

interface ErrorRepository : JpaRepository<Error, Long> {
    fun findByKey(key: String): Error
}

data class ErrorDto(
        val key: String,
        val message: Name,
        val status: Int,
        val datetime: Instant = Instant.now(),
        var details: MutableMap<String, Any?>? = null
)
