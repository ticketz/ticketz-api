package ticketz.config.exception

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.*
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.badRequest
import org.springframework.http.ResponseEntity.status
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import java.lang.reflect.UndeclaredThrowableException

fun findByKey(key: String?, errors: List<Error>, fallback: Error): Error =
        errors.find { it.key == key } ?: fallback

@ControllerAdvice
class ExceptionTranslator(errorRepository: ErrorRepository) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    private val errors = errorRepository.findAll()
    private val fallback = errorRepository.findByKey("internal.server.error")

    @ResponseBody
    @ExceptionHandler(ClientException::class)
    fun handleException(ex: ClientException): ResponseEntity<ErrorDto> {
        log.debug("client error: {}", ex.message)
        return badRequest()
                .body(findByKey(ex.message, errors, fallback).toDto(BAD_REQUEST))
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException::class)
    fun handleException(ex: NotFoundException): ResponseEntity<ErrorDto> {
        log.debug("not found error: {}", ex.message)
        return status(NOT_FOUND)
                .body(findByKey(ex.message, errors, fallback).toDto(NOT_FOUND))
    }


    @ResponseBody
    @ExceptionHandler(ServerException::class)
    fun handleServerException(ex: ServerException): ResponseEntity<ErrorDto> {
        log.error("server error: {}", ex.message, ex)
        return status(INTERNAL_SERVER_ERROR)
                .body(findByKey(ex.message, errors, fallback)
                        .toDto(INTERNAL_SERVER_ERROR))
    }

    @ResponseBody
    @ExceptionHandler(Exception::class)
    fun handleException(ex: Exception): ResponseEntity<ErrorDto> {
        log.error("exception: {}", ex.message, ex)
        return status(INTERNAL_SERVER_ERROR)
                .body(fallback.toDto(INTERNAL_SERVER_ERROR)
                        .apply {
                            details = mutableMapOf("cause" to ex.message)
                        }
                )
    }

    @ResponseBody
    @ExceptionHandler(UndeclaredThrowableException::class)
    fun handleException(ex: UndeclaredThrowableException) = when (ex.cause) {
        is ClientException -> handleException(ex.cause as ClientException)
        is NotFoundException -> handleException(ex.cause as NotFoundException)
        is ServerException -> handleException(ex.cause as ServerException)
        else -> handleException(ex.cause as Exception)
    }

    // TODO add bean validation exceptions

}
