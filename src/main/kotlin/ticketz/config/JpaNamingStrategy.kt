package ticketz.config

import org.hibernate.boot.model.naming.EntityNaming
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy

private const val ENTITY_SUFFIX_REGEX = "^(.+)$"
private const val LOOKUP_SUFFIX_REGEX = "^(.+)Lookup$"
private const val VIEW_SUFFIX_REGEX = "^(.+)View"

class JpaNamingStrategy : SpringImplicitNamingStrategy() {

    override fun transformEntityName(entityNaming: EntityNaming) =
            removeSuffix(super.transformEntityName(entityNaming))

    private fun removeSuffix(tableName: String) =
            tableName.replace(ENTITY_SUFFIX_REGEX.toRegex(), "$1")
                    .replace(LOOKUP_SUFFIX_REGEX.toRegex(), "lk_$1")
                    .replace(VIEW_SUFFIX_REGEX.toRegex(), "vw_$1")
}
