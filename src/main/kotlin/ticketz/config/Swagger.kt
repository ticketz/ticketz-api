package ticketz.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.util.MimeTypeUtils
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Configuration
@EnableSwagger2
@Profile("swagger")
@Import(BeanValidatorPluginsConfiguration::class)
class SwaggerConfig {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .directModelSubstitute(LocalDate::class.java, java.sql.Date::class.java)
                .directModelSubstitute(LocalDateTime::class.java, java.util.Date::class.java)
                .directModelSubstitute(LocalTime::class.java, String::class.java)
                .directModelSubstitute(Instant::class.java, java.util.Date::class.java)
                .apiInfo(apiInfo())
                .consumes(setOf(MimeTypeUtils.APPLICATION_JSON_VALUE))
                .produces(setOf(MimeTypeUtils.APPLICATION_JSON_VALUE))
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title("Ticketz REST API")
                .version("1")
                .build()
    }
}
