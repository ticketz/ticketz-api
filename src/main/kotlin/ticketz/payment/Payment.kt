package ticketz.payment

import ticketz.model.AbstractModel
import ticketz.reservation.Reservation
import java.time.Instant
import javax.persistence.Entity
import javax.persistence.OneToOne

@Entity
data class Payment(
        @OneToOne var reservation: Reservation,
        var datetime: Instant,
        var gatewayName: String
) : AbstractModel()
