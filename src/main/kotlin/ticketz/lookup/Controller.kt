package ticketz.lookup

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/lookup")
class LookupController(private val cityLookupRepository: CityLookupRepository) {

    @GetMapping("/cities")
    fun listCities() = cityLookupRepository.findAll()
}
