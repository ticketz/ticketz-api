package ticketz.lookup

import ticketz.model.AbstractLookup
import ticketz.model.LookupRepository
import ticketz.model.Name
import javax.persistence.Entity

@Entity
class CityLookup : AbstractLookup() {
    fun toDto() = CityDto(name)
}

data class CityDto(var name: Name?)

interface CityLookupRepository : LookupRepository<CityLookup>
