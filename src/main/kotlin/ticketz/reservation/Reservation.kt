package ticketz.reservation

import ticketz.event.Event
import ticketz.model.AbstractModel
import javax.persistence.Entity
import javax.persistence.ManyToOne

@Entity
data class Reservation(
        var mobileNumber: String,
        var email: String,
        var ticketCount: Int,
        @ManyToOne var event: Event,
        var paid: Boolean = false
) : AbstractModel()
