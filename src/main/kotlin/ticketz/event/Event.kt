package ticketz.event

import org.springframework.data.jpa.repository.JpaRepository
import ticketz.model.AbstractModel
import ticketz.model.Name
import java.time.LocalDateTime
import javax.persistence.*
import javax.persistence.AttributeOverride as Attr
import javax.persistence.AttributeOverrides as Attrs

@Embeddable
data class Actor(
        @Embedded
        @Attrs(Attr(name = "ar", column = Column(name = "actorNameAr")))
        var name: Name
) {
    fun toDto() = ActorDto(name)
}

@Embeddable
data class Image(
        val url: String,
        var displayOrder: Int? = null
) {
    fun toDto() = ImageDto(url, displayOrder)
}

@Entity
data class Event(
        @Embedded
        @Attrs(Attr(name = "ar", column = Column(name = "titleAr")))
        var title: Name,
        @Embedded
        @Attrs(Attr(name = "ar", column = Column(name = "descriptionAr")))
        var description: Name? = null,
        var datetime: LocalDateTime,
        var ticketCount: Int,
        @Embedded @ElementCollection
        var actors: Set<Actor>? = null,
        @Embedded @ElementCollection
        var images: Set<Image>? = null,
        @ManyToOne var location: Location,
        var price: Double
) : AbstractModel() {

    fun toDto() = EventDto(
            id!!,
            title,
            description,
            datetime,
            ticketCount,
            actors?.map { it.toDto() },
            images?.map { it.toDto() },
            location.toDto(),
            price)
}

data class ActorDto(var name: Name)

data class ImageDto(
        val url: String,
        var displayOrder: Int? = null
)

data class EventDto(
        var id: Long,
        var title: Name,
        var description: Name?,
        var datetime: LocalDateTime,
        var ticketCount: Int,
        var actors: List<ActorDto>?,
        var images: List<ImageDto>?,
        var location: LocationDto,
        var price: Double
)

interface EventRepository : JpaRepository<Event, Long>
