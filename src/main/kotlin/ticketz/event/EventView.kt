package ticketz.event

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ticketz.model.Name
import java.time.LocalDateTime
import javax.persistence.*
import javax.persistence.AttributeOverride as Attr
import javax.persistence.AttributeOverrides as Attrs

/**
 * View on [Event] entity used for search
 */
@Entity
data class EventView(
        @Id var id: Long,
        @Embedded
        @Attrs(Attr(name = "ar", column = Column(name = "titleAr")))
        var title: Name,
        var datetime: LocalDateTime,
        @Attrs(Attr(name = "ar", column = Column(name = "actorNamesAr")))
        var actors: Name?,
        var image: String?,
        var cityId: Long,
        @Embedded
        @Attrs(Attr(name = "ar", column = Column(name = "cityNameAr")))
        var city: Name
)

interface EventViewRepository : JpaRepository<EventView, Long> {

    @Query("select o from EventView o where o.cityId = :cityId or :cityId is null")
    fun findAllByCityId(cityId: Long?, pageable: Pageable): Page<EventView>
}
