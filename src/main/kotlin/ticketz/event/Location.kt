package ticketz.event

import ticketz.lookup.CityDto
import ticketz.lookup.CityLookup
import ticketz.model.AbstractModel
import ticketz.model.Name
import javax.persistence.*
import javax.persistence.AttributeOverride as Attr
import javax.persistence.AttributeOverrides as Attrs

@Embeddable
data class Point(
        var latitude: Double,
        var longitude: Double
)

@Entity
data class Location(
        @Embedded
        var point: Point?,
        @ManyToOne var city: CityLookup,
        @Embedded
        @Attrs(Attr(name = "ar", column = Column(name = "descriptionAr")))
        var description: Name? = null
) : AbstractModel() {
    fun toDto() = LocationDto(point, city.toDto(), description)
}

data class LocationDto(
        var point: Point?,
        var city: CityDto,
        var description: Name?
)
