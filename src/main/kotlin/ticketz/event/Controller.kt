package ticketz.event

import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

data class SearchParam(val cityId: Long?)

@RestController
@RequestMapping("/api/v1/events")
class EventController(private val eventService: EventService) {

    @GetMapping
    fun findAll(searchParam: SearchParam, pageable: Pageable) =
            eventService.findAll(searchParam.cityId, pageable)

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long) = eventService.findById(id)

}
