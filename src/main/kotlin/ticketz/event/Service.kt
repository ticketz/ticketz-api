package ticketz.event

import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ticketz.config.exception.NotFoundException

@Service
class EventService(
        private val eventViewRepository: EventViewRepository,
        private val eventRepository: EventRepository
) {

    fun findAll(cityId: Long?, pageable: Pageable) =
            eventViewRepository.findAllByCityId(cityId, pageable)

    @Transactional(readOnly = true)
    fun findById(id: Long): EventDto = eventRepository.findById(id)
            .orElseThrow { NotFoundException }.toDto()
}
