package ticketz

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import ticketz.util.sharedPostgreSQLContainer

@SpringBootTest
@Testcontainers
class ApplicationTests {

    companion object {
        @Container
        val postgreSQLContainer = sharedPostgreSQLContainer
    }

    @Test
    fun contextLoads() {
    }

}
