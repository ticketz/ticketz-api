package ticketz.config.exception

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import ticketz.model.Name

internal class ExceptionsKtTest {

    private val fallback = Error("key", Name(""))

    @Test
    fun `test findByKey will return default error in case of no match`() {
        val defaultError = findByKey("some.key", emptyList(), fallback)
        assertThat(defaultError).isEqualTo(fallback)
    }

    @Test
    fun `test findByKey will return find correctly`() {
        val message = Name("someName")
        val someKey = findByKey("some.key", arrayListOf(Error("some.key", message)), fallback)
        assertThat(someKey.message).isEqualTo(message)
    }
}
