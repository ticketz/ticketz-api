package ticketz.config

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.test.util.ReflectionTestUtils.invokeMethod

internal class JpaNamingStrategyTest {

    @Test
    fun testRemoveSuffixWillReturnSameValueWhenNoSuffixFound() {
        val result = invokeMethod<String>(JpaNamingStrategy(), "removeSuffix", "ABC")
        assertThat(result).isEqualTo("ABC")
    }

    @Test
    fun testRemoveSuffixWillRemoveIt() {
        val result = invokeMethod<String>(JpaNamingStrategy(), "removeSuffix", "ABC")
        assertThat(result).isEqualTo("ABC")
    }

    @Test
    fun testRemoveSuffixWillNotRemoveItIfItIsNotComingAtTheEnd() {
        val result = invokeMethod<String>(JpaNamingStrategy(), "removeSuffix", "ABCEntityXYZ")
        assertThat(result).isEqualTo("ABCEntityXYZ")
    }

    @Test
    fun testRemoveSuffixWillKeepTheSuffixIfItIsTheOnlyWord() {
        val result = invokeMethod<String>(JpaNamingStrategy(), "removeSuffix", "Entity")
        assertThat(result).isEqualTo("Entity")
    }

}
